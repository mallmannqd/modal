function editar(id){
    $('#modal').modal('show');
    $.ajax({
        url: '/index/buscaUsuario',
        type: 'POST',
        data: {
            id: id
        },
        dataType: 'json',
        beforeSend: function () {
            $('#formAtualizar').hide();
            $('#loading').show();
        },
        success: function (usuario) {

            $('#loading').hide();
            $('#formAtualizar').show();

            $('#id').val(usuario.id);
            $('#nome').val(usuario.nome);
            $('#email').val(usuario.email);
            $('#senha').val(usuario.senha);
        }
    });
}

$('#formAtualizar').on('submit', function (e) {

    let id = $('#id').val();
    let nome = $('#nome').val();
    let senha = $('#senha').val();
    let email = $('#email').val();

    e.preventDefault();
    $.ajax({
        url: '/index/atualizar',
        type: 'POST',
        data: {
            id: id,
            nome: nome,
            senha: senha,
            email: email
        },
        success: function (message) {
            alert(message);
            location.reload(true);
        }
    })
});

function excluir(){
    $('#modal').modal('show');
}