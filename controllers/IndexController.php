<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;
use models\Item;
use models\User;

class IndexController extends Controller
{
    public function index()
    {
        $user = new User();

        $dados['usuarios'] = $user->getUsers();

        $this->loadTemplate('index/index', $dados);
    }

    public function buscaUsuario(): void
    {
        $id = $_POST['id'];

        $user = new User();

        $user = $user->getUserById($id);

        echo (json_encode($user));
    }

    public function atualizar(): void
    {
        $usuario = new User();
        $usuario->update($_POST);
    }

}