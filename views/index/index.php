<h1>Usuários</h1>

<table class="table table-bordered">
    <?php foreach ($usuarios as $usuario) : ?>
        <tr>
            <td><?=$usuario->getNome()?></td>
            <td><?=$usuario->getEmail()?></td>
            <td><?=$usuario->getSenha()?></td>
            <td>
                <a href="javascript:;" onclick="editar(<?=$usuario->getId()?>)">Editar</a>
                <a href="javascript:;" onclick="excluir(<?=$usuario->getId()?>)">Excluir</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>

<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="loading" style="display: none;">
                    carregando...
                </div>
                <form id="formAtualizar">
                    <input type="hidden" id="id" name="id">
                    <label for="nome">Nome:</label>
                    <input class="form-control" id="nome" name="nome" type="text">

                    <label for="email">Email:</label>
                    <input class="form-control" id="email" name="email" type="email">

                    <label for="senha">Senha:</label>
                    <input class="form-control" id="senha" name="senha" type="text">

                    <button id="btnAtualiza" name="btnAtualiza" type="submit" class="btn btn-success">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>